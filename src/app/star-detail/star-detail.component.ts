import { Component, Input, OnInit } from '@angular/core';
import { Star } from '../star';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { StarService } from '../services/star.service';

@Component({
  selector: 'app-star-detail',
  templateUrl: './star-detail.component.html',
  styleUrls: ['./star-detail.component.scss']
})
export class StarDetailComponent implements OnInit {
  @Input() star?: Star;

  constructor(
    private route: ActivatedRoute,
    private starService: StarService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getStar();
  }

  getStar(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.starService.getStar(id)
      .subscribe(star => this.star = star);
  }

  save(): void {
    if (this.star) {
      this.starService.updateStar(this.star)
        .subscribe(() => this.goBack());
    }
  }

  goBack(): void {
    this.location.back();
  }
}

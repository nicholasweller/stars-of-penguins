import { Component, OnInit } from '@angular/core';

import { Star } from '../star';
import { StarService } from '../services/star.service';

@Component({
  selector: 'app-stars',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.scss']
})
export class StarsComponent implements OnInit {
  stars: Star[] = [];

  constructor(private starService: StarService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.starService.getStars()
    .subscribe(stars => this.stars = stars);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.starService.addStar({ name } as Star)
      .subscribe(star => {
        this.stars.push(star);
      });
  }

  delete(star: Star): void {
    this.stars = this.stars.filter(s => s !== star);
    this.starService.deleteStar(star.id).subscribe();
  }

}
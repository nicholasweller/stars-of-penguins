import { Star } from './star';

export const STARS: Star[] = [
  { id: 87, name: 'Crosby' },
  { id: 71, name: 'Malkin' },
  { id: 58, name: 'Letang' },
  { id: 13, name: 'Guerin' },
  { id: 44, name: 'Orpik' },
  { id: 29, name: 'Fleury' },
  { id: 35, name: 'Barasso' },
  { id: 68, name: 'Jagr' },
  { id: 77, name: 'Coffey' },
  { id: 66, name: 'Lemieux' },
  { id: 8, name: 'Recchi' }
];
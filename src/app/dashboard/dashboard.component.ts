import { Component, OnInit } from '@angular/core';
import { Star } from '../star';
import { StarService } from '../services/star.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.scss' ]
})
export class DashboardComponent implements OnInit {
  stars: Star[] = [];

  constructor(private starService: StarService) { }

  ngOnInit() {
    this.getStars();
  }

  getStars(): void {
    this.starService.getStars()
      .subscribe(stars => this.stars = stars.slice(1, 5));
  }
}